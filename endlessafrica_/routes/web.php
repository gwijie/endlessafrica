<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/survey', function () {
    return view('survey');
});

Route::get('/submitted', function () {
    return view('submitted');
});

Route::post('/submit', 'SurveyController@index');

Route::post('/submit_contact', 'ContactController@submit_contact');

