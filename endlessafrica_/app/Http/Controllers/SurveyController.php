<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SurveyResults;
use Carbon\Carbon;
use App\Survey;
use Session;

class SurveyController extends Controller
{
    public function index(Request $request){

        $fields = $request->all();
        
        $data = array(
            'name' => $fields['name'],
            'email' => $fields['email'],
            'gender' => $fields['gender'],
            'age' => $fields['age'],
            'house' => $fields['house'],
            'rent' => $fields['rent'],
            'live' => $fields['live'],
            'save' => $fields['save'],
            'access' => $fields['access'],
            'status' => 1,
            'date' => Carbon::now()
        ); 


        $i = Survey::insert($data);

        Mail::to($fields['email'])
              ->bcc('lacoasta@gmail.com')
              ->send(new SurveyResults($data));

        if($i > 0)
                {
                    Session::flash('messages','<div class="alert-success" style="padding: 5px;
                    margin: 7px 0 4px 0;
                    border-radius: 5px;
                    font-weight: bold;">
                    <center>Your results will be sent out to you email shortly</center></div>');
                    return redirect('/submitted');
                }

    }

}
