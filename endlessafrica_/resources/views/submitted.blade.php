<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ENDLESS AFRICA | Welcome </title>
        <!-- Plugins CSS -->
        <link href="{{ url('css/plugins/plugins.css')}}" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="{{ url('css/style.css')}}" rel="stylesheet">
    </head>
    <body data-spy="scroll" data-offset="58">
        <div id="preloader">
            <div id="preloader-inner"></div>
        </div><!--/preloader-->

        <nav class="navbar navbar-expand-lg navbar-inverse bg-inverse header-transparent fixed-top" style="background: black!important;">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="{{ url('images/logo.png')}}" alt="Endless Africa" width="150px;"></a>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a data-scroll class="nav-link active" href="/#home">Home</a></li>
                    <li class="nav-item"><a data-scroll class="nav-link" href="/#about">About</a></li>
                    <li class="nav-item"><a data-scroll class="nav-link" href="/#contact">Contact</a></li>                    
                    <li class="nav-item"><a class="nav-link bg-primary btn-rounded" href="#">Take a Test</a></li>
                </ul>
            </div>
        </nav><!--/.navbar-->
        
        <section id="contact" class="pt90 pb60 bg-faded">
            <div class="container">
                <div class="row" style="margin-top: 3em;">
                    <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3">
                        <div class="section-title">
                            <h4>Survey</h4>
                            
                            <?php echo Session::get('messages');?>
                            <br>

                            <a class="btn btn-primary" href="/">Take me home</a>
                            
                        </div>
                    </div>
                </div><!--/section title-->
               <div class="row">
                    <div class="col-md-12 mb30"></div>
            </div>
        </section><!--/.contact-->
        <div class="bg-primary contact-info pt50 pb20">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 mb30">
                        <div class="media">
                            <div class="d-flex mr-3">
                                <i class="ti-home"></i>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0">Address</h5>
                                Methodist Ministries Center, Block C, 3rd Floor<br> Nairobi, Kenya
                            </div>
                        </div>
                    </div><!--/.col-->
                    <div class="col-md-4 mb30">
                        <div class="media">
                            <div class="d-flex mr-3">
                                <i class="ti-headphone"></i>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0">Phone</h5>
                                +254 710 712 712<br>
                                +254 7xx xxx xxx
                            </div>
                        </div>
                    </div><!--/.col-->
                    <div class="col-md-4 mb30">
                        <div class="media">
                            <div class="d-flex mr-3">
                                <i class="ti-email"></i>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0">Email & website</h5>
                                support@endlessafrica.co.ke<br>
                                www.endlessafrica.co.ke
                            </div>
                        </div>
                    </div><!--/.col-->
                </div>
            </div>
        </div><!--/.contact-info-->
       
        <footer id="footer" class="footer  pt90 pb90">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a href="#"><i class="ti-facebook"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"><i class="ti-twitter"></i></a>
                            </li>
                            <li class="list-inline-item d-none">
                                <a href="#"><i class="ti-google"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"><i class="ti-instagram"></i></a>
                            </li>
                            <li class="list-inline-item d-none">
                                <a href="#"><i class="ti-dribbble"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"><i class="ti-linkedin"></i></a>
                            </li>
                        </ul>
                        <span>&copy; Copyright 2018. All Right Reserved.</span>
                    </div>
                </div>
            </div>
        </footer>
        <!--back to top-->
        <a href="#" class="back-to-top hidden-xs-down" id="back-to-top"><i class="ti-angle-up"></i></a>
        <!-- jQuery first, then Tether, then Bootstrap JS. -->
        <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js')}}"></script>
        <script src="{{ url('js/plugins/plugins.js')}}"></script> 
        <script src="{{ url('js/gems.custom.js')}}"></script> 

    </body>
</html>