@component('mail::message')
# Results are in

{{$data['name']}},<br>

Based on the information you provided, this is how you scored.

@component('mail::button', ['url' => 'http://endlessafrica.digitalatelier.co.ke'])
Take the Test Again
@endcomponent

Thanks,<br>
{{ config('app.name') }} Team
@endcomponent
