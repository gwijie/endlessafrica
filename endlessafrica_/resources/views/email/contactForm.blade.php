@component('mail::message')
# New Contact Message

From: {{$data['name']}}
<br>
Email Address: {{$data['email']}}
<br>
<br>
{{$data['msg']}}

Thanks,<br>
{{ config('app.name') }} Bot
@endcomponent
