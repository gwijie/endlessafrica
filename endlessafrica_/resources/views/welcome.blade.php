<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ENDLESS AFRICA | Welcome </title>
        <!-- Plugins CSS -->
        <link href="{{ url('css/plugins/plugins.css')}}" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="{{ url('css/style.css')}}" rel="stylesheet">
    </head>
    <body data-spy="scroll" data-offset="58">
        <div id="preloader">
            <div id="preloader-inner"></div>
        </div><!--/preloader-->

        <nav class="navbar navbar-expand-lg navbar-inverse bg-inverse header-transparent fixed-top">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="{{ url('images/logo.png')}}" alt="Endless Africa" width="150px;"></a>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a data-scroll class="nav-link active" href="#home">Home</a></li>
                    <li class="nav-item"><a data-scroll class="nav-link" href="#about">About</a></li>
                    <li class="nav-item"><a data-scroll class="nav-link" href="#contact">Contact</a></li>                    
                    <li class="nav-item"><a class="nav-link bg-primary btn-rounded" href="/survey">Take a Test</a></li>
                </ul>
            </div>
        </nav><!--/.navbar-->
        <section id='home' class="hero heroFullscreen bg-parallax" data-jarallax='{"speed": 0.2}' style='background-image: url("images/bg6.jpg")'>
            <div class='hero-overlay'></div>
            <div class="d-flex align-items-center">
                <div class="scroll-to hero-mouse"><a data-scroll="" href="#about"><i class="ti-angle-down"></i></a></div>
                <div class="container">
                    <div class="row">
                        <div class=" col-md-8  offset-md-2 text-center">
                            <h1 style="color: #ffcc00!important;">ENDLESS AFRICA</h1>
                            <h4 style="color: white;font-weight: normal!important;" class="col-md-12 ">Home ownership has become an elusive dream for many people in Kenya. 
                            We believe that there is a way for us to change this narrative…but first, take a test and let us find out how ready you are 
                            to start your home ownership journey</h4><br>
                            <ul class="hero-links center">
                           <li> <a class="btn col-md-4 center bg-primary btn-rounded" style="color: white;margin-bottom: 1em;" href="/survey">Take a Test</a> </li>
				
                           <li> <a class="btn col-md-4 center bg-primary btn-rounded" style="color: white;margin-bottom: 1em;" href="tel: *483*712#">Dial *483*712#</a> </li>
                           </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section><!--/.hero-->

        <section id="about" class="pt90 pb60">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3">
                        <div class="section-title">
                            <h4 style="color: #ffcc00!important;">About Us</h4>
                            <p class="lead d-none">libero ex dapibus diam expound the actual teachings of the great explorer of the truth</p>
                        </div>
                    </div>
                </div><!--/section title-->
            

                <div class="row">
                    <div class="col-md-4">
                    <h3 style="color: #ffcc00!important;">Our Mission </h3>
                        <p>Restoring human dignity</p>
                        <br>
                        <h3 style="color: #ffcc00!important;">Our Vision</h3>
                        <p>To build integrated and inclusive communities in Africa by providing a platform that encourages collaboration by different players in the housing sector to deliver on quality housing solutions that are accessible to the middle and low income population. </p>
                    </div>
                    <div class="col-md-8">
                    <h3 class="d-md-none d-lg-none d-xl-none">Endless Africa</h3>
                    <p class='mb40'>Home ownership has become an elusive dream for many people in Kenya but we believe that there is a way for us to change this narrative. 
As society we have observed that coming together on a common vision can result in collective success. This is evidenced in all the “Chamas” we know, community-based organizations (CBOs) that are uplifting people’s lives, table banking, merry-go-rounds, the list is endless... What if we came together to change the narrative for housing? What if together, we could manage to negotiate, structure and build our own communities? The housing sector has been characterized by a lot of inefficiencies, which have resulted in housing being too expensive for a lot of us to afford. 
It is this problem that Endless Africa Limited aims to solve. But to get there we need your help in getting information on the kind of houses that you would like to live in, how much can comfortably afford to pay, your expenditure on housing and an understanding of the general housing space. With this information, we intend to package a solution that is designed to suit you – affordable housing within a thriving community. So let’s start talking…</p>
                    </div>

                </div>

                <div class="row d-none">
                    <div class="col-md-4 mb30 wow zoomIn" data-wow-duration=".5s" data-wow-delay=".2s">
                        <div class="media icon-box">
                            <div class="d-flex mr-4">
                                <i class='ti-twitter'></i>
                            </div>
                            <div class="media-body">
                                <h4 class="mt-0 text-capitalize">Bootstrap <span class='text-primary'>4</span></h4>
                                <p class='mb0 text-small'>
                                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.                          
                                </p>
                            </div>
                        </div>
                    </div><!--/col-->
                    <div class="col-md-4 mb30 wow zoomIn" data-wow-duration=".5s" data-wow-delay=".4s">
                        <div class="media icon-box">
                            <div class="d-flex mr-4">
                                <i class='ti-mobile'></i>
                            </div>
                            <div class="media-body">
                                <h4 class="mt-0 text-capitalize"><span class='text-primary'>Mobile</span> Friendly</h4>
                                <p class='mb0 text-small'>
                                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.                          
                                </p>
                            </div>
                        </div>
                    </div><!--/col-->
                    <div class="col-md-4 mb30 wow zoomIn" data-wow-duration=".5s" data-wow-delay=".6s">
                        <div class="media icon-box">
                            <div class="d-flex mr-4">
                                <i class='ti-thumb-up'></i>
                            </div>
                            <div class="media-body">
                                <h4 class="mt-0 text-capitalize"><span class='text-primary'>Modern</span> Design</h4>
                                <p class='mb0 text-small'>
                                    Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.                          
                                </p>
                            </div>
                        </div>
                    </div><!--/col-->
                </div>
            </div>
        </section><!--about us-->
        <div class="parallax-cta pt90 pb60 bg-parallax" data-jarallax='{"speed": 0.2}' style='background-image: url("images/bg5.jpg")'>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 mb30 wow fadeIn" data-wow-duration=".5s" data-wow-delay=".2s">
                        <div class="media fun-facts">
                            <div class="d-flex align-self-center mr-4">
                                <i class='ti-money'></i>
                            </div>
                            <div class="media-body">
                                <h3 class="mt-0 text-uppercase display-4 text-white" style="color: #ffcc00!important;">30 %</h3>
                                <p class='mb0 text-small text-uppercase'>Household Income towards Rent
                                </p>
                            </div>
                        </div>
                    </div><!--/col-->
                    <div class="col-lg-3 col-md-6 mb30 wow fadeIn" data-wow-duration=".5s" data-wow-delay=".4s">
                        <div class="media fun-facts">
                            <div class="d-flex align-self-center mr-4">
                                <i class='ti-home'></i>
                            </div>
                            <div class="media-body">
                                <h3 class="mt-0 text-uppercase display-4 text-white" style="color: #ffcc00!important;">85,000</h3>
                                <p class='mb0 text-small text-uppercase'>Deficit to meet demand annually
                                </p>
                            </div>
                        </div>
                    </div><!--/col-->
                    <div class="col-lg-3 col-md-6 mb30 wow fadeIn" data-wow-duration=".5s" data-wow-delay=".6s">
                        <div class="media fun-facts">
                            <div class="d-flex align-self-center mr-4">
                                <i class='ti-paint-roller'></i>
                            </div>
                            <div class="media-body">
                                <h3 class="mt-0 text-uppercase display-4 text-white" style="color: #ffcc00!important;">88.6%</h3>
                                <p class='mb0 text-small text-uppercase'>Dwellings constructed by households themselves
                                </p>
                            </div>
                        </div>
                    </div><!--/col-->
                    <div class="col-lg-3 col-md-6 mb30 wow fadeIn" data-wow-duration=".5s" data-wow-delay=".8s">
                        <div class="media fun-facts">
                            <div class="d-flex align-self-center mr-4">
                                <i class='ti-credit-card'></i>
                            </div>
                            <div class="media-body">
                                <h3 class="mt-0 text-uppercase display-4 text-white" style="color: #ffcc00!important;">90%</h3>
                                <p class='mb0 text-small text-uppercase'>financial institutions do not have products geared towards savings for home ownership
                                </p>
                            </div>
                        </div>
                    </div><!--/col-->
                </div>
            </div>      
        </div><!--/.fun facts-->
        
        <section id="contact" class="pt90 pb60 bg-faded">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3">
                        <div class="section-title">
                            <h4 style="color: #ffcc00!important;">Contact Us</h4>
                            <p class="lead d-none">libero ex dapibus diam expound the actual teachings of the great explorer of the truth</p>
                        </div>
                    </div>
                </div><!--/section title-->
               <div class="row">
                    <div class="col-md-6 mb30">
                        
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.8053468523863!2d36.77419341475401!3d-1.2911279990582993!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f1a0f29c564a5%3A0xae14d246bc6bd339!2sMethodist+Ministries+Centre!5e0!3m2!1sen!2ske!4v1526860022686" class="mb0" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                    <div class="col-md-6" id="hello">
                        <h4 class="text-capitalize">Say Hello!</h4>
                        <hr>
                        <form method="post" action="submit_contact" data-toggle="validator" class="">
                        {{csrf_field()}}
                            <div class="row">
                                <div class="col-sm-12">                                 
                                    <div class=" mb20">
                                        <input type="text" name="name" class="form-control" placeholder="Full Name...." required/>
                                    </div>
                                    <div class=" mb20">
                                        <input type="email" name="email" class="form-control" placeholder="Email Address...." required/>
                                    </div>                          
                                </div>
                                <div class="col-sm-12">
                                    <textarea name="msg" class="form-control mb20" rows="5" placeholder="Message...." required></textarea>                                  
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <?php echo Session::get('messages');?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <button type="submit" class="btn btn-lg btn-primary">Send Message</button>
                                </div>
                            </div>
                        </form><!--form end-->
                    </div>
                </div>

            </div>
        </section><!--/.contact-->
        <div class="bg-primary contact-info pt50 pb20">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 mb30">
                        <div class="media">
                            <div class="d-flex mr-3">
                                <i class="ti-home"></i>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0">Address</h5>
                                Methodist Ministries Center, Block C, 3rd Floor<br> Nairobi, Kenya
                            </div>
                        </div>
                    </div><!--/.col-->
                    <div class="col-md-4 mb30">
                        <div class="media">
                            <div class="d-flex mr-3">
                                <i class="ti-headphone"></i>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0">Phone</h5>
                                +254 710 712 712<br>
                                +254 7xx xxx xxx
                            </div>
                        </div>
                    </div><!--/.col-->
                    <div class="col-md-4 mb30">
                        <div class="media">
                            <div class="d-flex mr-3">
                                <i class="ti-email"></i>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0">Email & website</h5>
                                support@endlessafrica.co.ke<br>
                                www.endlessafrica.co.ke
                            </div>
                        </div>
                    </div><!--/.col-->
                </div>
            </div>
        </div><!--/.contact-info-->
        <div class="bottom-links container-fluid d-none">
            <div class="row align-items-center">
                <a class="col-md-6 links-col bg-dark wow zoomIn" data-wow-delay=".2s" data-wow-duration=".3s" href="#">
                    <span class="text-uppercase">Plan a Project</span>
                    Hire us
                </a>
                <a class="col-md-6 links-col secondary-col wow zoomIn" data-wow-delay=".4s" data-wow-duration=".5s" href="#">
                    <span class="text-uppercase">Work with us</span>
                    Join the team
                </a>
            </div>
        </div><!--/.bottom links-->
        <footer id="footer" class="footer  pt90 pb90">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a href="#"><i class="ti-facebook"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"><i class="ti-twitter"></i></a>
                            </li>
                            <li class="list-inline-item d-none">
                                <a href="#"><i class="ti-google"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"><i class="ti-instagram"></i></a>
                            </li>
                            <li class="list-inline-item d-none">
                                <a href="#"><i class="ti-dribbble"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"><i class="ti-linkedin"></i></a>
                            </li>
                        </ul>
                        <span>&copy; Copyright 2018. All Right Reserved.</span>
                    </div>
                </div>
            </div>
        </footer>
        <!--back to top-->
        <a href="#" class="back-to-top hidden-xs-down" id="back-to-top"><i class="ti-angle-up"></i></a>
        <!-- jQuery first, then Tether, then Bootstrap JS. -->
        <script src="{{ url('js/plugins/plugins.js')}}"></script> 
        <script src="{{ url('js/gems.custom.js')}}"></script> 
    </body>
</html>