<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ENDLESS AFRICA | Welcome </title>
        <!-- Plugins CSS -->
        <link href="{{ url('css/plugins/plugins.css')}}" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="{{ url('css/style.css')}}" rel="stylesheet">
    </head>
    <body data-spy="scroll" data-offset="58">
        <div id="preloader">
            <div id="preloader-inner"></div>
        </div><!--/preloader-->

        <nav class="navbar navbar-expand-lg navbar-inverse bg-inverse header-transparent fixed-top" style="background: black!important;">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="{{ url('images/logo.png')}}" alt="Endless Africa" width="150px;"></a>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item"><a data-scroll class="nav-link active" href="/#home">Home</a></li>
                    <li class="nav-item"><a data-scroll class="nav-link" href="/#about">About</a></li>
                    <li class="nav-item"><a data-scroll class="nav-link" href="/#contact">Contact</a></li>                    
                    <li class="nav-item"><a class="nav-link bg-primary btn-rounded" href="#">Take a Test</a></li>
                </ul>
            </div>
        </nav><!--/.navbar-->
        
        <section id="contact" class="pt90 pb60 bg-faded">
            <div class="container">
                <div class="row" style="margin-top: 3em!important;">
                    <div class="col-md-8 col-lg-6 offset-md-2 offset-lg-3">
                        <div class="section-title">
                            <h4>Survey</h4>
                            <p class="lead">Spare less than a minute to take this short survey</p>
                            
                            <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="17" aria-valuemin="17" aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div><!--/section title-->
               <div class="row">
                    <div class="col-md-12 mb30">
                        <form method="post" action="/submit" class="">
                        {{csrf_field()}}
                            <div class="row">
                                <div class="col-sm-12"> 
                                <div class="house">
                                    <h3>What type of house are you?</h3><br>
                                        <div class="form-row" style="margin-left: 2em;">
                                            
                                            <div class="col">
                                            <input class="form-check-input" checked="checked" type="radio" name="house" id="inlineRadio1" value="Mansion">
                                            <label class="form-check-label" for="inlineRadio1"> Mansion &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="{{ url('images/mansion.png')}}" alt="" ></label>
                                            </div>
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="house" id="inlineRadio1" value="Town House">
                                            <label class="form-check-label" for="inlineRadio1">Town House &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="{{ url('images/townhouse.png')}}" alt="" ></label>
                                            </div>
                                        </div> 
                                        <div class="form-row" style="margin-left: 2em;">
                                            
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="house" id="inlineRadio1" value="Gated Community">
                                            <label class="form-check-label" for="inlineRadio1">Gated Community <img src="{{ url('images/gatedcommunity.png')}}" alt="" ></label>
                                            </div>
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="house" id="inlineRadio1" value="Apartment">
                                            <label class="form-check-label" for="inlineRadio1">Apartment &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img src="{{ url('images/apartment.png')}}" alt="" ></label>
                                            </div>
                                        </div>

                                        <div class="form-row" style="margin-left: 2em;">
                                            
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="house" id="inlineRadio1" value="Thatched House">
                                            <label class="form-check-label" for="inlineRadio1">Thatched House &nbsp;&nbsp;&nbsp;&nbsp;<img src="{{ url('images/hut.png')}}" alt="" ></label>
                                            </div>
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="house" id="inlineRadio1" value="Country Home">
                                            <label class="form-check-label" for="inlineRadio1">Country Home &nbsp;&nbsp;<img src="{{ url('images/country-house.png')}}" alt="" ></label>
                                            </div>
                                        </div>

                                    
                                    <div class="col-sm-12 text-right">
                                        
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="nextHouse">Next</button>
                                    </div>
                                    <br>
                                </div>

                                <div class="live">
                                    <h3>Where would you like to live?</h3><br>
                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                            <input class="form-check-input zonea" type="radio" checked="checked" name="live" id="inlineRadio1" value="Donholm"> 
                                            <label class="form-check-label" for="inlineRadio1">Donholm</label>

                                                <!-- Modal -->
                                                <div class="modal fade" id="zonea" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalCenterTitle">Average Cost of Property: Donholm</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <b>Apartment</b> 
                                                                <p>Cost: 6M<br>Deposit: 1.2M</p>
                                                                <hr>
                                                                <b>Mansion</b> 
                                                                <p>Cost: 60M<br>Deposit: 12M</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End Modal -->
                                        </div>

                                        <div class="col">
                                            <input class="form-check-input zoneb" type="radio" name="live" id="inlineRadio1" value="Kileleshwa">
                                            <label class="form-check-label" for="inlineRadio1">Kileleshwa</label>

                                                <!-- Modal -->
                                                <div class="modal fade" id="zoneb" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalCenterTitle">Average Cost of Property: Kileleshwa</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <b>Apartment</b> 
                                                                <p>Cost: 6M<br>Deposit: 1.2M</p>
                                                                <hr>
                                                                <b>Mansion</b> 
                                                                <p>Cost: 60M<br>Deposit: 12M</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End Modal -->
                                        </div>
                                    </div> 
                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input zonec" type="radio" name="live" id="inlineRadio1" value="Muthaiga">
                                        <label class="form-check-label" for="inlineRadio1">Muthaiga</label>

                                                 <!-- Modal -->
                                                 <div class="modal fade" id="zonec" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalCenterTitle">Average Cost of Property: Muthaiga</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <b>Apartment</b> 
                                                                <p>Cost: 6M<br>Deposit: 1.2M</p>
                                                                <hr>
                                                                <b>Mansion</b> 
                                                                <p>Cost: 60M<br>Deposit: 12M</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End Modal -->
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input zoned" type="radio" name="live" id="inlineRadio1" value="Kahawa">
                                        <label class="form-check-label" for="inlineRadio1">Kahawa</label>

                                                 <!-- Modal -->
                                                 <div class="modal fade" id="zoned" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalCenterTitle">Average Cost of Property: Kahawa</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <b>Apartment</b> 
                                                                <p>Cost: 6M<br>Deposit: 1.2M</p>
                                                                <hr>
                                                                <b>Mansion</b> 
                                                                <p>Cost: 60M<br>Deposit: 12M</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End Modal -->

                                        </div>
                                    </div>

                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input zonee" type="radio" name="live" id="inlineRadio1" value="Balozi">
                                        <label class="form-check-label" for="inlineRadio1">Balozi</label>

                                                 <!-- Modal -->
                                                 <div class="modal fade" id="zonee" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalCenterTitle">Average Cost of Property: Balozi</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <b>Apartment</b> 
                                                                <p>Cost: 6M<br>Deposit: 1.2M</p>
                                                                <hr>
                                                                <b>Mansion</b> 
                                                                <p>Cost: 60M<br>Deposit: 12M</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End Modal -->
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input zonef" type="radio" name="live" id="inlineRadio1" value="Fedha">
                                        <label class="form-check-label" for="inlineRadio1">Fedha</label>

                                                 <!-- Modal -->
                                                 <div class="modal fade" id="zonef" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalCenterTitle">Average Cost of Property: Fedha</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <b>Apartment</b> 
                                                                <p>Cost: 6M<br>Deposit: 1.2M</p>
                                                                <hr>
                                                                <b>Mansion</b> 
                                                                <p>Cost: 60M<br>Deposit: 12M</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End Modal -->
                                        </div>
                                    </div> 
                                    <div class="col-sm-12 text-right">
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="prevLive">Previous</button>
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="nextLive">Next</button>
                                    </div>
                                    <br>
                                </div>
                            
                                <div class="rent">    
                                    <h3>How much are you currently paying for rent?</h3><br>
                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" checked="checked" name="rent" id="inlineRadio1" value="2K - 7K">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 2,000 - 7,000</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="rent" id="inlineRadio1" value="9K - 15K">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 9,000 - 15,000</label>
                                        </div>
                                    </div> 
                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="rent" id="inlineRadio1" value="18K - 28K">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 18,000 - 28,000</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="rent" id="inlineRadio1" value="32K - 40K">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 32,000 - 40,000</label>
                                        </div>
                                    </div>

                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="rent" id="inlineRadio1" value="45K - 60K">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 45,000 - 60,000</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="rent" id="inlineRadio1" value="65K - 90K">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 65,000 - 90,000</label>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 text-right">
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="prevRent">Previous</button>
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="nextRent">Next</button>
                                    </div> 
                                    <br>
                                </div>
                                
                                <div class="save">
                                    <h3>Over and above your monthly rent or if you are the lucky few who are not currently paying rent. How much are you able to save towards a deposit?</h3><br>
                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" checked="checked" name="save" id="inlineRadio1" value="2K - 5K">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 2,000 - 5,000</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="save" id="inlineRadio1" value="6K - 10K">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 6,000 - 10,000</label>
                                        </div>
                                    </div> 
                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="save" id="inlineRadio1" value="11K - 15K">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 11,000 - 15,000</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="save" id="inlineRadio1" value="16K - 20K">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 16,000 - 20,000</label>
                                        </div>
                                    </div>

                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="save" id="inlineRadio1" value="21K - 30K">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 21,000 - 30,000</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="save" id="inlineRadio1" value="31K - 40K">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 31,000 - 40,000</label>
                                        </div>
                                    </div> 

                                    <div class="form-row" style="margin-left: 2em;">
                                        
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="save" id="inlineRadio1" value="41K - 50K">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 41,000 - 50,000</label>
                                        </div>
                                        <div class="col">
                                        <input class="form-check-input" type="radio" name="save" id="inlineRadio1" value="51K - 60K<">
                                        <label class="form-check-label" for="inlineRadio1">Kshs. 51,000 - 60,000</label>
                                        </div>
                                    </div>  
                                    <div class="col-sm-12 text-right">
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="prevSave">Previous</button>
                                        <button type="button" name="submit" class="btn btn-lg btn-primary" id="nextSave">Next</button>
                                    </div>
                                    <br>
                                </div>
                                    <div class="access"> 
                                        <h3>Which of the following options can you access aditionally to raise your deposit?</h3><br>
                                        <div class="form-row" style="margin-left: 2em;">
                                            
                                            <div class="col">
                                            <input class="form-check-input" type="radio" checked="checked" name="access" id="inlineRadio1" value="Informal Loan">
                                            <label class="form-check-label" for="inlineRadio1">Informal Loan</label>
                                            </div>
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="access" id="inlineRadio1" value="Formal Loan">
                                            <label class="form-check-label" for="inlineRadio1">Formal Loan</label>
                                            </div>
                                        </div> 
                                        <div class="form-row" style="margin-left: 2em;">
                                            
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="access" id="inlineRadio1" value="Personal Savings">
                                            <label class="form-check-label" for="inlineRadio1">Personal Savings</label>
                                            </div>
                                            <div class="col">
                                            <input class="form-check-input" type="radio" name="access" id="inlineRadio1" value="Can sell other assets">
                                            <label class="form-check-label" for="inlineRadio1">Can sell other assets</label>
                                            </div>
                                            <div class="col-sm-12 text-right">
                                            <button type="button" name="submit" class="btn btn-lg btn-primary" id="prevAccess">Previous</button>
                                            <button type="button" name="submit" class="btn btn-lg btn-primary" id="nextAccess">Next</button>
                                            </div>
                                        </div> 
                                    </div>

                                    <div class="submit"> 
                                        <h3>Enter your details to get your score</h3><br>
                                        <div class="form-row" style="margin-left: -1em;">
                                            
                                            <div class="col-sm-12">                                 
                                                <div class=" mb20">
                                                    <input type="text" name="name" class="form-control" placeholder="Full Name...." required/>
                                                </div>
                                                <div class=" mb20">
                                                    <input type="text" name="email" class="form-control" placeholder="Email Address...." required/>
                                                </div>
                                                <div class=" mb20">
                                                <h3>Age</h3><br>
                                                <div class="form-row" style="margin-left: 2em;">
                                                        <div class="col">
                                                        <input class="form-check-input" type="radio" checked="checked" name="age" id="inlineRadio1" value="18 - 25">
                                                        <label class="form-check-label" for="inlineRadio1">18 - 25 Yrs</label>
                                                        </div>
                                                        <div class="col">
                                                        <input class="form-check-input" type="radio" name="age" id="inlineRadio1" value="26 - 35">
                                                        <label class="form-check-label" for="inlineRadio1">26 - 35 Yrs</label>
                                                        </div>
                                                    </div> 
                                                    <div class="form-row" style="margin-left: 2em;">
                                                        
                                                        <div class="col">
                                                        <input class="form-check-input" type="radio" name="age" id="inlineRadio1" value="36 - 45">
                                                        <label class="form-check-label" for="inlineRadio1">36 - 45 Yrs</label>
                                                        </div>
                                                        <div class="col">
                                                        <input class="form-check-input" type="radio" name="age" id="inlineRadio1" value="46 - 55">
                                                        <label class="form-check-label" for="inlineRadio1">46 - 55 Yrs</label>
                                                        </div>
                                                    </div> 
                                                </div> 
                                                <div class=" mb20">
                                                <h3>Gender</h3><br>
                                                    <select name="gender" class="form-control">
                                                        <option value="male">Male</option>
                                                        <option value="female">Female</option>
                                                    </select>
                                                </div>                          
                                            </div>

                                            <div class="col-sm-12 text-right">
                                                <button type="button" name="submit" class="btn btn-lg btn-primary" id="prevSubmit">Previous</button>
                                                <button type="submit" name="submit" class="btn btn-lg btn-primary" id="nextAge">Finish</button>
                                            </div>
                                        </div> 
                                    </div>
                                    
                            </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-12 text-center">
                                    <div class="data-status"></div> <!-- data submit status -->
                                    
                                </div>
                            </div>
                        </form><!--form end-->
                    </div>
                </div>

            </div>
        </section><!--/.contact-->
        <div class="bg-primary contact-info pt50 pb20">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 mb30">
                        <div class="media">
                            <div class="d-flex mr-3">
                                <i class="ti-home"></i>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0">Address</h5>
                                Methodist Ministries Center, Block C, 3rd Floor<br> Nairobi, Kenya
                            </div>
                        </div>
                    </div><!--/.col-->
                    <div class="col-md-4 mb30">
                        <div class="media">
                            <div class="d-flex mr-3">
                                <i class="ti-headphone"></i>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0">Phone</h5>
                                +254 710 712 712<br>
                                +254 7xx xxx xxx
                            </div>
                        </div>
                    </div><!--/.col-->
                    <div class="col-md-4 mb30">
                        <div class="media">
                            <div class="d-flex mr-3">
                                <i class="ti-email"></i>
                            </div>
                            <div class="media-body">
                                <h5 class="mt-0">Email & website</h5>
                                support@endlessafrica.co.ke<br>
                                www.endlessafrica.co.ke
                            </div>
                        </div>
                    </div><!--/.col-->
                </div>
            </div>
        </div><!--/.contact-info-->
       
        <footer id="footer" class="footer  pt90 pb90">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a href="#"><i class="ti-facebook"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"><i class="ti-twitter"></i></a>
                            </li>
                            <li class="list-inline-item d-none">
                                <a href="#"><i class="ti-google"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"><i class="ti-instagram"></i></a>
                            </li>
                            <li class="list-inline-item d-none">
                                <a href="#"><i class="ti-dribbble"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#"><i class="ti-linkedin"></i></a>
                            </li>
                        </ul>
                        <span>&copy; Copyright 2018. All Right Reserved.</span>
                    </div>
                </div>
            </div>
        </footer>
        <!--back to top-->
        <a href="#" class="back-to-top hidden-xs-down" id="back-to-top"><i class="ti-angle-up"></i></a>
        <!-- jQuery first, then Tether, then Bootstrap JS. -->
        <script src="{{ url('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js')}}"></script>
        <script src="{{ url('js/plugins/plugins.js')}}"></script> 
        <script src="{{ url('js/gems.custom.js')}}"></script> 

    </body>
</html>